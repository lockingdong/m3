<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    protected $fillable = [ //在這裡面的欄位才能被新增
        'url', 'title1', 'title2', 'd_img', 'm_img', 'on',
    ];
}
