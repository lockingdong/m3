<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Handlers\ImageUploadHandler;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('backend.global.banners');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ImageUploadHandler $uploader, Banner $banner)
    {
        //
        //return 123;

        //dd($request->on);
        $data = $request->all();
        if ($request->d_img) {
            $result = $uploader->save($request->d_img, 'banners', '');
            if ($result) {
                $data['d_img'] = $result['path'];
            }
        }
        if ($request->m_img) {
            $result = $uploader->save($request->m_img, 'banners', '');
            if ($result) {
                $data['m_img'] = $result['path'];
            }
        }

        $banner->fill($data);
        $banner->save();

        //return redirect()->to($topic->link())->with('success', '帖子创建成功！');
        return 'succ';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(banner $banner)
    {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, banner $banner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(banner $banner)
    {
        //
    }
}
