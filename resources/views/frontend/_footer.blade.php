{{-- footer --}}
<footer class="container-fluid p-0">
    <div class="row wow fadeIn">
        <div class="col-lg-9 col-12 pt-lg-5 pt-4">
            <div class="row ">
                <div class="col-lg-4 col-6 mx-auto mt-lg-0 mt-2 text-lg-center mt-lg-0 mt-2 text-nowrap">
                    <a href="#">關於BMW MOBILE</a>
                </div>
                <div class="col-lg-2 col-6 mx-auto mt-lg-0 mt-2 ">
                    <a href="#">常見問題</a>
                </div>
                <div class="col-lg-2 col-6 mx-auto mt-lg-0 mt-2 ">
                    <a href="terms.php">網站條款</a>
                </div>
                <div class="col-lg-2 col-6 mx-auto mt-lg-0 mt-2 ">
                    <a href="">隱私權政策</a>
                </div>
                <div class="col-lg-2 col-6 m-auto mt-lg-0 mt-2 ">
                    <a href="#">企業合作</a>
                </div>
                <div class="col-lg-2 col-6   mx-auto mt-lg-0 mt-2 icon-desktop">
                    <div class="row">
                        <a href="#"> <i class="fab fa-facebook-f"></i> </a>
                        <a href="#"> <i class="fab fa-instagram"></i> </a>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-2 col-6  icon-mobile pt-lg-5 pt-4">
            <div class="row ">
                <div class="col-12  mx-auto d-flex  mx-auto mt-lg-0 mt-2 ">
                    <div class="row ">
                        <a href="#" class="text-center d-inline-block"> <i class="fab fa-facebook-f "></i> </a>
                        <a href="#" class="d-inline-block"> <i class="fab fa-instagram"></i> </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 copy text-center text-white text-uppercase">©2018 Moveon-design. All rights reserved </div>
    </div>
</footer>
{{-- footer --}}