{{-- nav bar --}}
<div class="container-fluid top"></div>
<div class="container-fluid p-0 main-bar fixed-top">
    <nav class="navbar navbar-expand-xl navbar-light bmw-bg-dark py-3 ">
        <a class="navbar-brand px-xl-3  " href="/">
            <img src="src/img/LOGO.svg" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto d-flex  navbar-nav-desktop  justify-content-between">
                <div class="move-box"></div>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text" href="news.php">最新消息</a>
                </li>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text" href="#">熱門活動</a>
                </li>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text" href="fixed-forum.php">維修討論</a>
                </li>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text" href="modified.php">改裝分享</a>
                </li>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text " href="#">廠商專區</a>
                </li>
                <li class="nav-item my-auto  move text-nowrap py-xl-0 pt-2">
                    <a class="nav-link bmw-navbar-text" href="secondHand-forum.php">二手專區</a>
                </li>

            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item my-auto  mr-xl-1 search py-xl-0 pt-2 order-xl-1  order-2">
                    <div class="input-group mb-3 bg-dark my-auto  search-sm">
                        <div class="input-group-prepend border-0 bmw-bg-dark ">
                            <span class="input-group-text bmw-bg-dark border-0 bmw-navbar-text" i><i class="fas fa-search" style="cursor:pointer"></i></span>
                        </div>
                        <input type="text" class="form-control bmw-bg-dark p-0 show " placeholder="SEARCH">
                    </div>
                </li>
                <li class="nav-item my-auto text-nowrap py-xl-0 pt-2 order-xl-2 order-1">
                    <a class="nav-link bmw-navbar-text " href="login.php">登入/註冊</a>
                </li>
                <!-- login show -->
                <!-- <li class="nav-item my-auto text-nowrap py-xl-0 pt-2">
        <a class="nav-link bmw-navbar-text " href="login.php" >個人資訊</a>
        </li>
        <li class="nav-item my-auto text-nowrap py-xl-0 pt-2">
        <a class="nav-link bmw-navbar-text " href="login.php" >登出</a>
        </li> -->
            </ul>
        </div>
    </nav>
</div>
{{-- nav bar --}}