<!-- Optional JavaScript -->
<!-- jquery 3.3.1 -->
<script src="/backend/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
<!-- bootstap bundle js -->
<script src="/backend/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
<!-- slimscroll js -->
<script src="/backend/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
<!-- main js -->
<script src="/backend/assets/libs/js/main-js.js"></script>
<!-- chart chartist js -->
<script src="/backend/assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
<!-- sparkline js -->
<script src="/backend/assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
<!-- morris js -->
<script src="/backend/assets/vendor/charts/morris-bundle/raphael.min.js"></script>
<script src="/backend/assets/vendor/charts/morris-bundle/morris.js"></script>
<!-- chart c3 js -->
<script src="/backend/assets/vendor/charts/c3charts/c3.min.js"></script>
<script src="/backend/assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
<script src="/backend/assets/vendor/charts/c3charts/C3chartjs.js"></script>
<script src="/backend/assets/libs/js/dashboard-ecommerce.js"></script>