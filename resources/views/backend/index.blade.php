<!doctype html>
<html lang="en">
 
<head>
    @include('backend.subview._head')
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        @include('backend.subview._navbar')
        @include('backend.subview._sidebar')


        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    @include('backend.subview._pageheader')


                    @yield('content')
                    
                </div>
            </div>
            

            @include('backend.subview._footer')
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    @include('backend.subview._scripts')

    @yield('script')
    
</body>

</html>