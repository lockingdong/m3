(function () {
  $.validator.setDefaults({
    debug: true
  })
  //  填寫資料註冊頁
  $.validator.addMethod("regex", function (value, element, param) {
    // 強制加上 ^ 與 $ 符號限制整串文字都要符合
    return value.match(new RegExp("^" + param + "$"));
  });


  $("#info-validate-form").validate({
    errorPlacement: function (error, element) {
      console.log(element.parent().parent().find('label'));
      element.parent().parent().find('label').addClass('d-block');
      element.parent().parent().find('label').addClass('error');
    },
    success: function (error, element) {
      error.addClass('d-none');
    },
    rules: {
      username: {
        required: true,
        minlength: 2,
      },
      nickname: {
        required: true,
        minlength: 2,
        regex: "[\u4e00-\u9fa5_a-zA-Z0-9]+",
      },
      email: {
        required: true,
        // email: true
      },
      ownCar: {
        required: true,
      },
      password: {
        regex: "[A-Za-z0-9]+",
        required: true,
        minlength: 8,
      },
      checkpw: {
        required: true,
        equalTo: "#password"
      }

    },

    messages: {
      username: '2-10字以內，供日後查詢使用',
      nickname: {
        required: '此處不可空白',
        minlength: '2-10字以內，請勿使用特殊符號',
        regex: '2-10字以內，請勿使用特殊符號',

      },
      email: {
        required: "We need your email address to contact you",
        email: "Your email address must be in the format of name@domain.com"
      },
      ownCar: {
        required: '此處不可空白',
      },
      password: {
        regex: '只能使用英數字',
        required: '此處不可空白',
        minlength: '最少8字以上並包含英數字',
      },
      checkpw: {
        required: '與設定的密碼不符',
        equalTo: "與設定的密碼不符"
      }
    },

    submitHandler: function (form) {
      form.submit();
    }
  });
  //信箱註冊
  $("#email-validate-form").validate({
    errorPlacement: function (error, element) {
      console.log(element.parent().parent().find('label'));
      element.parent().parent().find('label').addClass('d-block');
      element.parent().parent().find('label').addClass('error');
    },
    success: function (error, element) {
      error.addClass('d-none');
    },
    rules: {
      email: 'required',
    },
    submitHandler: function (form) {
      form.submit();
    }
  });
  $("#profile-edit-form").validate({
    errorPlacement: function (error, element) {
      console.log(element.parent().parent().find('label'));
      element.parent().parent().find('label').addClass('d-block');
      element.parent().parent().find('label').addClass('error');
    },
    success: function (error, element) {
      error.addClass('d-none');
    },
    rules: {
      username: {
        required: true,
        minlength: 2,
      },
      nickname: {
        required: true,
        minlength: 2,
        maxlength: 10,
        // alphanumeric: true
        regex: "[\u4e00-\u9fa5_a-zA-Z0-9]+",
      },
      email: {
        required: true,
        // email: true
      },
      ownCar: {
        required: true,
      },
      password: {
        regex: "[A-Za-z0-9]+",
        required: true,
        minlength: 8,
      },
      checkpw: {
        required: true,
        equalTo: "#password"
      },
      facebook: {
        url: true,
      },
      ig: {
        url: true
      }

    },

    messages: {
      username: '2-10字以內，供日後查詢使用',
      nickname: {
        required: '此處不可空白',
        minlength: '2-10字以內，請勿使用特殊符號',
        minlength: '2-10字以內，請勿使用特殊符號',
        regex: '2-10字以內，請勿使用特殊符號',

      },
      email: {
        required: "We need your email address to contact you",
        email: "Your email address must be in the format of name@domain.com"
      },
      ownCar: {
        required: '此處不可空白',
      },
      password: {
        regex: '只能使用英數字',
        required: '此處不可空白',
        minlength: '最少8字以上並包含英數字',
      },
      checkpw: {
        required: '與設定的密碼不符',
        equalTo: "與設定的密碼不符"
      },
      facebook: {
        url: '網址錯誤'
      },
      ig: {
        url: '網址錯誤'
      }
    },

    submitHandler: function (form) {
      form.submit();
    }
  });
})();