@extends('backend.index')


{{-- 頁面標題 --}}
@section('page-title')
    BANNER管理
@endsection

@section('content')
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="section-block" id="basicform">
            <h3 class="section-title">新增banner</h3>
            <p>新增banner</p>
        </div>
        <div class="card">
            <h5 class="card-header">banner資訊</h5>
            <div class="card-body">
                <form method="POST" action="{{route('banner.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="url" class="col-form-label">連結</label>
                        <input name="url" id="url" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title1">標題1</label>
                        <input name="title1" id="title1" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="title2">標題2</label>
                        <input name="title2" id="title2" type="text" class="form-control">
                    </div>
                    {{-- 桌機圖 --}}
                    <div class="form-group">
                        <label for="d_img">桌機圖</label>
                        <div class="custom-file">
                            <input name="d_img" type="file" class="custom-file-input" id="d_img">
                            <label class="custom-file-label" for="d_img">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <img id="d_img_pre" class="mw-100">
                    </div>

                    {{-- 手機圖 --}}
                    <div class="form-group">
                        <label for="m_img">手機圖</label>
                        <div class="custom-file">
                            <input name="m_img" type="file" class="custom-file-input" id="m_img">
                            <label class="custom-file-label" for="m_img">Choose file</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <img id="m_img_pre" class="mw-100">
                    </div>

                    <div class="form-group row">
                        <label class="col-12 col-sm-1 col-form-label">上架</label>
                        <div class="col-12 col-sm-8 col-lg-6 pt-1">
                            <div class="switch-button switch-button-yesno">
                                <input type="hidden" name="on" value="0">
                                <input type="checkbox" checked value="1" name="on" id="on"><span>
                                <label for="on"></label></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-12">
                            <div class="text-right">
                                <button class="btn btn-primary" type="submit">送出</button>

                            </div>
                        </div>
                    </div>
                    {{-- <a href="#" class="btn btn-primary">Primary</a> --}}
                    
                    
                    
                </form>
            </div>
            
        </div>
    </div>
</div>
    
@endsection


@section('script')
<script>

$("input#on").on('change', function() {
    if ($(this).is(':checked')) {
        $(this).attr('value', 1);
        $(this).prop('checked', true)
    }
});


function readURL(input ,el) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
        $(el).attr('src', e.target.result);
    }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#d_img").change(function() {
    readURL(this, '#d_img_pre');
});

$("#m_img").change(function() {
    readURL(this, '#m_img_pre');
});


</script>

    
@endsection