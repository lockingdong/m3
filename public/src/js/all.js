(function () {
  // ------------------------------------
  // ------------------------------------
  // navbar 
  //***********************************
  //***********************************
  // top 
  let $h = $('.main-bar').height();
  $('.top').height($h);
  console.log($h);
  //***********************************
  //***********************************
  // serach bar function  

  function searchbar() {
    $('.nav-item .form-control').toggleClass('show');
  }

  //***********************************
  //***********************************  
  // moveLine 
  // let hue = 0;
  // 滑入產生border 
  function movein() {

    $(".move-box").css({
      'height': `${$(".navbar").outerHeight()}px`,
      'width': `${$(this).outerWidth()}px`,
      'opacity': 1,
    });
    let d = $(this).offset().left - $(".navbar-nav").offset().left;
    $(".move-box").css("left", `${d}px`);
  }
  // 滑出淡出
  function moveout() {
    $(".move-box").css("opacity", "0");
  }
  //滑動切換顏色
  function move() {
    // hue += 2;
    // console.log(hue);
    // if (hue >= 360) {
    //   hue = 0;
    // }
    $(".move-box").css({
      'border-top': `4px solid #00eaff`,
      'border-bottom': `3px solid #00eaff`,
    });
  }
  // 點擊打開serach bar
  $('.fa-search').on('click', searchbar);
  //判斷螢幕大於1200監聽事件
  if ($(window).width() > 1200) {
    $(".navbar .move").on('mouseover', movein);
    $(".navbar .move").on('mousemove', move);
    $(".navbar-nav").on('mouseout', moveout);
  }
  //字數限字
  function limit($limit) {
    var $len = $limit; // 超過50個字以"..."取代
    $(".card .card-text").each(function () {
      if ($(this).text().length > $len) {
        var $text = $(this).text().substring(0, $len - 1) + ".....";
        $(this).text($text);
      }
    });
  }
  limit(50);
  // 首頁遮罩


  function closedialog() {
    $('.mask').css('width', '0%');
    $('.modal-activity').css({
      'opacity': 0,
      'display': 'none',
    });
  }
  $('.close').on('click', closedialog)



  // 啟動wow
  new WOW().init();
  // ------------------------------------
  // ------------------------------------
  // main-index slick 


  $(".slider").slick({
    autoplay: false,
    dots: true,
    arrows: false,
    fade: true,
    swipeToSlide: true,
    cssEase: 'linear',
    customPaging: function (slider, i) {
      let num = i + 1;
      return '<a class="mt-sm-0 mt-3">' + num + '</a>';
    },
  });
  $('.car-slider').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    swipeToSlide: true,
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  $('.modified-label-slider').slick({
    infinite: false,
    arrows: false,
    dots: false,
    slidesToShow: 1,
    swipeToSlide: true,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,

      }
    }, {
      breakpoint: 769,
      settings: {
        slidesToShow: 3,
        infinite: true,

      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        infinite: true,
      }
    }]
  })
  $('.modified-info-slider').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 4,
    swipeToSlide: true,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 1400,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  console.log('abc');

  $('.article-mobile-slick-news').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    autoplay: true,
    speed: 500,
    responsive: [{
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        fade: true,
      }
    }]
  })
  // ------------------------------------
  // ------------------------------------
  //profile-view
  $('.profile-pic').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    swipeToSlide: true,
    infinite: false,
    responsive: [{
      breakpoint: 1440,
      settings: {
        slidesToShow: 2,
      }
    }, {
      breakpoint: 769,
      settings: {
        slidesToShow: 1,
      }
    }]
  });
  // profile-edit
  $('.profile-edit-slick').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    swipeToSlide: true,
    infinite: false,
    responsive: [{
        breakpoint: 1500,
        settings: {
          slidesToShow: 2,
          arrows: true
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          arrows: true
        }
      },
      {
        breakpoint: 670,
        settings: {
          slidesToShow: 1,
          arrows: true
        }
      },
    ]
  });
})();