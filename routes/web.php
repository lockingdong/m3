<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});



Route::get('/m3-admin', function () {
    return view('backend.index');
});

Route::get('/m3-admin/banners/create', 'HomeController@create')->name('banner.create');
Route::post('/m3-admin/banners/store', 'HomeController@store')->name('banner.store');

