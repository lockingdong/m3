<!DOCTYPE html>
<html lang="en">

    @include('frontend._head')

<body>
    @include('frontend._navbar')

    <!--  header banner  -->
    <header class="container-fluid p-0 main-banner">
        <div id="maincarousel" class="carousel slide " data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
                <div class="carousel-item active" onclick="window.open('http://google.com')">
                    <img class="w-100 banner-desktop" src="src/img/banner01.jpg" alt="Bmw-desktop">
                    <img class="w-100 banner-mobile" src="src/img/banner_M01.jpg" alt="Bmw-mobile">
                    <div class="carousel-caption w-25">
                        <div class="carousel-caption-block">
                            <p class="m-0 text-nowrap">無可比擬的操作靈敏度</p>
                            <p class="m-0 text-nowrap">極致完美的駕乘樂趣</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="w-100 banner-desktop" src="src/img/banner02.jpg" alt="Bmw-car-desktop">
                    <img class="w-100 banner-mobile" src="src/img/banner_M02.jpg" alt="Bmw-car-mobile">
                    <div class="carousel-caption w-25">
                        <div class="carousel-caption-block">
                            <p class="m-0 text-nowrap">無可比擬的操作靈敏度</p>
                            <p class="m-0 text-nowrap">極致完美的駕乘樂趣</p>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="w-100 banner-desktop" src="src/img/banner03.jpg" alt="BMW-logo-desktop">
                    <img class="w-100 banner-mobile" src="src/img/banner_M03.jpg" alt="BMW-logo-mobile">
                    <div class="carousel-caption w-25">
                        <div class="carousel-caption-block">
                            <p class="m-0 text-nowrap">無可比擬的操作靈敏度</p>
                            <p class="m-0 text-nowrap">極致完美的駕乘樂趣</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev justify-content-start" href="#maincarousel" role="button" data-slide="prev">
                <div class="fa-icon">
                    <i class="fa fa-angle-left"></i>
                </div>
            </a>
            <a class="carousel-control-next justify-content-end" href="#maincarousel" role="button" data-slide="next">
                <div class="fa-icon">
                    <i class="fa fa-angle-right"></i>
                </div>
            </a>
        </div>
    </header>

    <!-- 最新消息 -->
    <section class="section1 pt-3   ">
        <div class="col-12 p-0 animated fadeIn">
            <div class="row py-5  ">
                <div class="col-12">
                    <h6 class="text-white text-center  ">NEWS <span> <br class="title-break">最新消息</span></h6>
                </div>
            </div>
            <div class="row py-3">
                <div class="col-xl-8 col-12 mx-auto p-0">
                    <section class="slider text-center ">
                        <div class="slider-item">
                            <div class="new-banner">
                                <a href="news-article.php"><img src="src/img/News01.jpg" alt="#"></a>
                            </div>
                            <div class="new-info">
                                <div class="new-header">
                                    <p class=" m-0 text-nowrap text-white">無可比擬的操作靈敏度</p>
                                    <p class=" m-0 text-nowrap text-white">極致完美的駕乘樂趣</p>
                                </div>
                                <p class="text-white  pt-3  text-sm-left w-75">內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文</p>
                                <p class="text-right text-uppercase w-75"><a href="#">see more</a></p>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="new-banner">
                                <a href="news-article.php"><img src="src/img/News02.jpg" alt="#"></a>
                            </div>
                            <div class="new-info">
                                <div class="new-header">
                                    <p class=" m-0 text-nowrap text-white">無可比擬的操作靈敏度</p>
                                    <p class=" m-0 text-nowrap text-white">極致完美的駕乘樂趣</p>
                                </div>
                                <p class="text-white  pt-3  text-sm-left w-75">內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文</p>
                                <p class="text-right text-uppercase"><a href="#">see more</a></p>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="new-banner">
                                <a href="news-article.php"> <img src="src/img/News03.jpg" alt="#"></a>
                            </div>
                            <div class="new-info">
                                <div class="new-header">
                                    <p class=" m-0 text-nowrap text-white">無可比擬的操作靈敏度</p>
                                    <p class=" m-0 text-nowrap text-white">極致完美的駕乘樂趣</p>
                                </div>
                                <p class="text-white  pt-3  text-sm-left w-75">內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文</p>
                                <p class="text-right text-uppercase"><a href="#">see more</a></p>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="new-banner">
                                <a href="news-article.php"> <img src="src/img/News02.jpg" alt="#"></a>
                            </div>
                            <div class="new-info">
                                <div class="new-header">
                                    <p class=" m-0 text-nowrap text-white">無可比擬的操作靈敏度</p>
                                    <p class=" m-0 text-nowrap text-white">極致完美的駕乘樂趣</p>
                                </div>
                                <p class="text-white  pt-3  text-sm-left w-75">內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文</p>
                                <p class="text-right text-uppercase"><a href="#">see more</a></p>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="new-banner">
                                <a href="news-article.php"> <img src="src/img/News01.jpg" alt="#"></a>
                            </div>
                            <div class="new-info">
                                <div class="new-header">
                                    <p class=" m-0 text-nowrap text-white">無可比擬的操作靈敏度</p>
                                    <p class=" m-0 text-nowrap text-white">極致完美的駕乘樂趣</p>
                                </div>
                                <p class="text-white  pt-3  text-sm-left">內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文</p>
                                <p class="text-right text-uppercase"><a href="#">see more</a></p>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!-- 車型 -->
    <section class="car-model pt-3">
        <div class="col-12 p-0  wow fadeInUp ">
            <div class="row py-5">
                <div class="col-12">
                    <h6 class="text-white text-center">POPULAR ACTIVITIES <span> <br class="title-break">熱門活動</span></h6>
                </div>
            </div>
            <div class="row py-3">
                <div class="col-12 p-lg-0">
                    <section class="car-slider m-auto">
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top" src="src/img/mode01.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">限時優惠</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top  img-fluid" src="src/img/mode02.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">OMPETITION PACKAGE競技</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top  img-fluid" src="src/img/mode03.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">見照片中BMW從後方追撞富豪汽車，造成BMW</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top  img-fluid" src="src/img/mode04.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">BMW台灣總代理汎德官方網站提供BMW德國豪華進口汽車最新</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top  img-fluid" src="src/img/mode05.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">預約試駕</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item">
                            <div class="card  rounded-0 border-0">
                                <a href=""> <img class="card-img-top  img-fluid " src="src/img/mode01.jpg" alt="Card image cap"></a>
                                <div class="card-body">
                                    <a href="#">
                                        <p class="car-name m-0 text-center text-truncate">E36 M3</p>
                                        <p class="card-text text-center">COMPETITION PACKAGE競技化套件</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    </section>
    <!-- 維修討論 -->
    <section class="forum-area pt-3">
        <div class="col-12 p-0 wow fadeInUp">
            <div class="row py-5">
                <div class="col-12">
                    <h6 class="text-white text-center ">MAINTENACE DISCUSSION <span><br class="title-break">維修討論</span></h6>
                </div>
            </div>
            <div class="col-lg-8 col-12 mx-auto py-3 ">
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-top-lael p-1">
                            <span class="badge  rounded-0  ">TOP</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">M3/M4是一台車M3/M4是一台車M3/M4是一台車</p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-02</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">這是名字這是名字</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-hot-lael p-1">
                            <span class="badge rounded-0 ">HOT</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">M4超好開 CP值爆表 心得分享</p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-30</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">藤原填海</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-essence-lael p-1">
                            <span class="badge rounded-0 ">精華</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">BMW M3 玩命關頭最夯車種及各車性能超級詳細解析 趕快來討論
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-30</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">馮迪索</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-new-lael p-1">
                            <span class="badge rounded-0 ">NEW</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">【圖】BMW/寶馬- M3 汽車價格,新款車型,規格配備,評價,深度解析
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-12-31</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">秋名山神主牌</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-new-lael p-1">
                            <span class="badge rounded-0 ">NEW</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">【海外新訊】BMW M3 CS誕生，1200輛限定販售M族菁英部隊！
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-01-01</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">0857</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <span class="badge   rounded-0  align-middle p-2"></span>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">馬力直上450 匹！小改BMW M3 / M4 標配「競技套件」登台發表
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-01-01</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">三峽彭于晏</p>
                    </div>
                </div>
                <div class="row py-5 forum-more">
                    <div class="col-sm-6 col-12 m-auto text-center"> <a href="fixed-article.php" class="py-2 ">其他更多討論</a> </div>
                </div>
            </div>
        </div>
    </section>

    <!-- 改裝分享 -->
    <section class="modified-area pt-3">
        <div class="col-12 p-0 wow fadeInUp">
            <div class="row py-5">
                <div class="col-12">
                    <h6 class="text-center ">MODIFIED SHARING<span><br class="title-break">改裝分享</span></h6>
                </div>
            </div>
            <div class="row py-2">
                <div class="col-lg-10 col-12 m-auto">
                    <section class="modified-label-slider text-center mx-auto">
                        <div class="slider-item m-auto ">
                            <input type="radio" name="options" class="invisible" id="option1">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option1">M3雙門跑車車</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option2">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option2">M4</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option3">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option3">M5</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option4">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option4">M6</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option5">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option5">M7</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option6">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option6">M8</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option7">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option7">M9</label>
                        </div>
                        <div class="slider-item m-auto">
                            <input type="radio" name="options" class="invisible" id="option8">
                            <label class="btn btn-secondary  modified-slider-item  px-4 py-1" for="option8">M10</label>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row py-3">
                <div class="col-xl-10 col-12 p-lg-0 mx-auto">
                    <section class="modified-info-slider m-auto">
                        <div class="slider-item mx-auto  ">
                            <div class="card mx-auto ">
                                <a href=""><img class="card-img-top img-fluid" src="src/img/mode01.jpg" alt="Card image cap"></a>
                                <div class="card-body ">
                                    <h5 class="card-title text-center">多功能旅行套裝組</h5>
                                    <p class="card-text  ">抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 </p>
                                    <p class="card-time mb-0"> <span class="d-inline-block pt-2 ">2018</span>.04.04</p>
                                    <p class="card-time "> 作者：胡適</p>
                                    <p class="text-center"> <a href="fixed-article.php" class="btn  rounded-0  px-4 ">SEE MORE</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item mx-auto  ">
                            <div class="card mx-auto ">
                                <a href=""><img class="card-img-top img-fluid" src="src/img/banner_M02.jpg" alt="Card image cap"></a>
                                <div class="card-body ">
                                    <h5 class="card-title text-center">多功能旅行套裝組</h5>
                                    <p class="card-text  ">抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 </p>
                                    <p class="card-time mb-0"> <span class="d-inline-block pt-2 ">2018</span>.04.04</p>
                                    <p class="card-time "> 作者：朱自清</p>
                                    <p class="text-center"> <a href="fixed-article.php" class="btn  rounded-0  px-4 ">SEE MORE</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item mx-auto  ">
                            <div class="card mx-auto ">
                                <a href=""><img class="card-img-top img-fluid" src="src/img/banner_M01.jpg" alt="Card image cap"></a>
                                <div class="card-body ">
                                    <h5 class="card-title text-center">多功能旅行套裝組</h5>
                                    <p class="card-text  ">抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 </p>
                                    <p class="card-time mb-0"> <span class="d-inline-block pt-2 ">2018</span>.04.04</p>
                                    <p class="card-time "> 作者：蘇軾</p>
                                    <p class="text-center"> <a href="fixed-article.php" class="btn  rounded-0  px-4 ">SEE MORE</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="slider-item mx-auto  ">
                            <div class="card mx-auto ">
                                <a href=""><img class="card-img-top img-fluid" src="src/img/banner_M02.jpg" alt="Card image cap"></a>
                                <div class="card-body ">
                                    <h5 class="card-title text-center">多功能旅行套裝組</h5>
                                    <p class="card-text  ">抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 抱著灰雞上飛機，飛機起飛，灰雞要飛，飛機墜機，灰雞歸西。 </p>
                                    <p class="card-time mb-0"> <span class="d-inline-block pt-2 ">2018</span>.04.04</p>
                                    <p class="card-time "> 作者：徐志摩</p>
                                    <p class="text-center"> <a href="fixed-article.php" class="btn  rounded-0  px-4 ">SEE MORE</a></p>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
            <div class="row py-5 forum-more">
                <div class="col-sm-6 col-12 m-auto text-center"> <a href="fixed-article.php" class="py-2 ">其他更多分享</a> </div>
                <!-- <div class="col-sm-6 col-12 m-auto text-center">  <div class=""></div> <a href="fixed-article.php" class="">其他更多分享</a> </div> -->
            </div>
        </div>
    </section>
    <!-- 二手專區 -->
    <section class="forum-area forum-second-hand pt-3">
        <div class="col-12 p-0 wow fadeInUp">
            <div class="row py-5">
                <div class="col-12">
                    <h6 class="text-white text-center ">USED AREA <br class="title-break">二手專區</span></h6>
                </div>
            </div>
            <div class="col-lg-8 col-12 mx-auto py-3 ">
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-top-lael p-1">
                            <span class="badge rounded-0 ">TOP</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">M3/M4是一台車M3/M4是一台車M3/M4是一台車</p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-02</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">這是名字這是名字</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-hot-lael p-1">
                            <span class="badge rounded-0 ">HOT</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">M4超好開 CP值爆表 心得分享</p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-30</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">藤原填海</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-essence-lael p-1">
                            <span class="badge rounded-0 ">精華</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">BMW M3 玩命關頭最夯車種及各車性能超級詳細解析 趕快來討論
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-02-30</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">馮迪索</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-new-lael p-1">
                            <span class="badge rounded-0 ">NEW</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">【圖】BMW/寶馬- M3 汽車價格,新款車型,規格配備,評價,深度解析
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-12-31</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">秋名山神主牌</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <div class="label-item forum-new-lael p-1">
                            <span class="badge rounded-0 ">NEW</span>
                        </div>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">【海外新訊】BMW M3 CS誕生，1200輛限定販售M族菁英部隊！
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-01-01</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">0857</p>
                    </div>
                </div>
                <div class="row py-3 forum-content ">
                    <div class="col-sm-1 col-2  text-white d-flex justify-content-center align-items-center">
                        <span class="badge   rounded-0  align-middle p-2"></span>
                    </div>
                    <div class="forum-title col-sm-8 col-10 text-white d-flex  align-items-center  text-truncate ">
                        <a href="fixed-article.php" class="d-block text-truncate">
                            <p class=" align-middle m-0  text-truncate ">馬力直上450 匹！小改BMW M3 / M4 標配「競技套件」登台發表
                            </p>
                        </a>
                    </div>
                    <div class="forum-info col-sm-2  col-8   offset-sm-0 offset-2 text-white d-flex  justify-content-center flex-column pr-0  ">
                        <p class="text-left text-nowrap mb-0">2018-01-01</p>
                        <p class="text-left mb-0 d-inline-block text-truncate">三峽彭于晏</p>
                    </div>
                </div>
                <div class="row py-5 forum-more">
                    <div class="col-sm-6 col-12 m-auto text-center"> <a href="fixed-article.php" class="py-2 ">其他更多二手</a> </div>
                </div>
            </div>
        </div>
    </section>

    {{-- @include('frontend._pop-up') --}}

    @include('frontend._footer')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
        <script src="/src/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" defer></script>
        <script src="/src/js/all.js" defer></script>
        <script defer>
        </script>
    </body>

</html>